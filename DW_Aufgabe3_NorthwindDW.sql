--DW erstellen, Aufgabe 3



create database NorthwindDW;
go

use NorthwindDW


--DIMENSIONEN ENTWICKELN

---------------------------------------------------------------------
CREATE TABLE DimTime
(
	DimTimeID			INT	IDENTITY PRIMARY KEY NONCLUSTERED, -- Immer daran denken, jede Dimension ben�tigt ihren eigenen Key
	[Quarter]			INT	NOT NULL,
	[Year]				INT	NOT NULL,
	[Month]				INT NOT NULL
);


INSERT DimTime
SELECT 
			DISTINCT
			DATEPART(QUARTER,OrderDate) Quartal,
			DATEPART(YEAR,OrderDate) Jahr,
			DATEPART(MONTH,OrderDate) Monat
	FROM 
		Northwind..Orders;

---------------------------------------------------------------------
CREATE TABLE DimCountry
(
	DimCountryID	INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	CountryIDOriginal	VARCHAR(15)	
);

INSERT DimCountry
	SELECT DISTINCT	
		ShipCountry 
	 FROM
		 [Northwind].[dbo].[Orders] ;

-----------------------------------------------------------------------

CREATE TABLE DimCustomer
(
	DimCustomerID		INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	CustomerIDOriginal	VARCHAR(10)
);

INSERT DimCustomer
SELECT	
		CustomerID

	FROM 
		[Northwind].[dbo].[Customers];
-------------------------------------------------------------------------
CREATE TABLE DimEmployee
(
	DimEmployeeID	INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	EmployeeIDOriginal	VARCHAR(10)	
);


INSERT DimEmployee
SELECT	
		EmployeeID

	FROM 
		[Northwind].[dbo].[Orders];

-------------------------------------------------------------------------


CREATE TABLE DimShipper
(
	DimShipperID		INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	ShipperIDOriginal	VARCHAR(10)
);


INSERT DimShipper
SELECT	
		ShipperID

	FROM 
		[Northwind].[dbo].Shippers;

-------------------------------------------------------------------------
-------------------------------------------------------------------------

--FAKTENTABELLEN ENTWICKELN


--1. Es soll ermittelt werden k�nnen, wie viele Bestellungen ein Mitarbeiter pro Monat, Quartal
--und Jahr bearbeitet hat!


CREATE TABLE FactEmpTime
(
	DimTimeID		INT,
	DimEmployeeID	INT,
	SummeOrders		INT
	PRIMARY KEY (DimTimeID, DimEmployeeID),
	FOREIGN KEY (DimTimeID) REFERENCES DimTime(DimTimeID),
	FOREIGN KEY (DimEmployeeID) REFERENCES DimEmployee(DimEmployeeID)
);

INSERT FactEmpTime
	SELECT
			dt.DimTimeID,
			e.DimEmployeeID,
			COUNT(o.EmployeeID) as Anzahl
	 FROM [Northwind].[dbo].[Orders] as o
	 INNER JOIN DimTime AS dt 
		ON dt.Quarter = DATEPART(Quarter,o.OrderDate) AND dt.Year = DATEPART(YEAR,o.OrderDate) AND dt.Month = DATEPART(month,o.OrderDate)
	INNER JOIN DimEmployee as e ON o.EmployeeID = e.EmployeeIDOriginal
		GROUP BY dt.DimTimeID, e.DimEmployeeID
		ORDER BY DimEmployeeID , DimTimeID;
	  
-------------------------------------------------------------------------
--Erweitern Sie das Modell aus der vorherigen Aufgabe so, dass auch noch ermittelt werden
--kann, welche Anzahl von Bestellungen ein Mitarbeiter pro Kunde bearbeitet hat.



CREATE TABLE FactEmpCustOrders
(
	DimCustomerID		INT,
	DimEmployeeID	INT,
	SummeOrders		INT
	PRIMARY KEY (DimCustomerID, DimEmployeeID),
	FOREIGN KEY (DimCustomerID) REFERENCES DimCustomer(DimCustomerID),
	FOREIGN KEY (DimEmployeeID) REFERENCES DimEmployee(DimEmployeeID)
);
	

INSERT FactEmpCustOrders
SELECT
			c.DimCustomerID,
			e.DimEmployeeID, 
			COUNT(o.EmployeeID) as Anzahl
	 FROM [Northwind].[dbo].[Orders] as o
	INNER JOIN DimCustomer as c on c.CustomerIDOriginal = o.CustomerID
	INNER JOIN DimEmployee as e ON o.EmployeeID = e.EmployeeIDOriginal
		GROUP BY  e.DimEmployeeID, c.DimCustomerID
		ORDER BY DimEmployeeID , c.DimCustomerID ;

-------------------------------------------------------------------------
--Au�er den oben genannten Informationen m�chte das Unternehmen noch auswerten
--k�nnen, welche Kosten f�r Lieferungen angefallen sind. Es soll ermittelt werden k�nnen,
--welche Kosten pro Monat, Quartal und Jahr f�r den Versand �ber einen Versender (Shipper)
--angefallen sind.




CREATE TABLE FactShipCostTime
(
	DimTimeID		INT,
	DimShipperID	INT,
	ShippingCosts	MONEY
	PRIMARY KEY (DimTimeID, DimShipperID),
	FOREIGN KEY (DimTimeID) REFERENCES DimTime(DimTimeID),
	FOREIGN KEY (DimShipperID) REFERENCES DimShipper(DimShipperID)
);

	
INSERT FactShipCostTime
	SELECT
			dt.DimTimeID,
			s.DimShipperID,
			SUM(o.Freight) as Kosten
	 FROM [Northwind].[dbo].[Orders] as o
	 INNER JOIN DimTime AS dt 
		ON dt.Quarter = DATEPART(Quarter,o.OrderDate) AND dt.Year = DATEPART(YEAR,o.OrderDate) AND dt.Month = DATEPART(month,o.OrderDate)
	INNER JOIN DimShipper as s ON o.ShipVia = s.ShipperIDOriginal
		GROUP BY s.DimShipperID, dt.DimTimeID
		ORDER BY s.DimShipperID, dt.DimTimeID;
	

-------------------------------------------------------------------------


--Au�erdem soll ausgewertet werden k�nnen, wie viele Lieferungen in ein bestimmtes Land
--gegangen sind. 



CREATE TABLE FactCountryOrders
(
	DimCountryID	INT PRIMARY KEY,
	Anzahl			INT
	FOREIGN KEY (DimCountryID) REFERENCES DimCountry(DimCountryID),
)

INSERT FactCountryOrders
SELECT
	co.DimCountryID,
	COUNT(o.OrderID) as Anzahl
 FROM [Northwind].[dbo].[Orders] as o
 INNER JOIN DimCountry as co
	ON co.CountryIDOriginal = o.ShipCountry
GROUP BY co.DimCountryID
ORDER BY co.DimCountryID;
	
	
-------------------------------------------------------------------------		