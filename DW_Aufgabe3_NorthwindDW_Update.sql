--DW erstellen, Aufgabe 3



create database NorthwindDW2;
go

use NorthwindDW2


--DIMENSIONEN ENTWICKELN

---------------------------------------------------------------------
CREATE TABLE DimTime
(
	DimTimeID			INT	IDENTITY PRIMARY KEY NONCLUSTERED, -- Immer daran denken, jede Dimension ben�tigt ihren eigenen Key
	[Quarter]			INT	NOT NULL,
	[Year]				INT	NOT NULL,
	[Month]				INT NOT NULL
);


INSERT DimTime
SELECT 
			DISTINCT
			DATEPART(QUARTER,OrderDate) Quartal,
			DATEPART(YEAR,OrderDate) Jahr,
			DATEPART(MONTH,OrderDate) Monat
	FROM 
		Northwind..Orders;

---------------------------------------------------------------------
CREATE TABLE DimCountry
(
	DimCountryID	INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	CountryIDOriginal	VARCHAR(15)	
);

INSERT DimCountry
	SELECT DISTINCT	
		ShipCountry 
	 FROM
		 [Northwind].[dbo].[Orders] ;

-----------------------------------------------------------------------

CREATE TABLE DimCustomer
(
	DimCustomerID		INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	CustomerIDOriginal	VARCHAR(10)
);

INSERT DimCustomer
SELECT DISTINCT	
		CustomerID

	FROM 
		[Northwind].[dbo].[Customers];
-------------------------------------------------------------------------
CREATE TABLE DimEmployee
(
	DimEmployeeID	INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	EmployeeIDOriginal	VARCHAR(10)	
);


INSERT DimEmployee
SELECT DISTINCT	
		EmployeeID

	FROM 
		[Northwind].[dbo].[Employees];

-------------------------------------------------------------------------


CREATE TABLE DimShipper
(
	DimShipperID		INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	ShipperIDOriginal	VARCHAR(10)
);


INSERT DimShipper
SELECT	
		ShipperID

	FROM 
		[Northwind].[dbo].Shippers;

-------------------------------------------------------------------------
-------------------------------------------------------------------------

--FAKTENTABELLEN ENTWICKELN


--1. Es soll ermittelt werden k�nnen, wie viele Bestellungen ein Mitarbeiter pro Monat, Quartal
--und Jahr bearbeitet hat!


CREATE TABLE FactEmpTime
(
	DimTimeID		INT,
	DimEmployeeID	INT,
	DimCustomerID		INT,
	DimShipperID	INT,
	DimCountryID	INT,
	PRIMARY KEY (DimTimeID, DimEmployeeID, DimCustomerID, DimShipperID, DimCountryID),
	SummeOrdersEmpTime		INT,
	--SummeOrdersEmpCust		INT,
	--AnzahlCountryOrders			INT
	ShippingCosts	MONEY,
	FOREIGN KEY (DimTimeID) REFERENCES DimTime(DimTimeID),
	FOREIGN KEY (DimEmployeeID) REFERENCES DimEmployee(DimEmployeeID),
	FOREIGN KEY (DimCustomerID) REFERENCES DimCustomer(DimCustomerID),
	FOREIGN KEY (DimShipperID) REFERENCES DimShipper(DimShipperID),
	FOREIGN KEY (DimCountryID) REFERENCES DimCountry(DimCountryID)
);


INSERT FactEmpTime
	SELECT
			dt.DimTimeID,
			e.DimEmployeeID,
			c.DimCustomerID,
			s.DimShipperID,
			co.DimCountryID,
			COUNT(o.EmployeeID) as AnzahlEmpTime,
			--COUNT(o.EmployeeID) as AnzahlCustEmp,
			--COUNT(o.OrderID) as AnzahlOrdersCountry,
			SUM(o.Freight) as Kosten
	 FROM [Northwind].[dbo].[Orders] as o
	 INNER JOIN DimTime AS dt 
		ON dt.Quarter = DATEPART(Quarter,o.OrderDate) AND dt.Year = DATEPART(YEAR,o.OrderDate) AND dt.Month = DATEPART(month,o.OrderDate)
	INNER JOIN DimEmployee as e ON o.EmployeeID = e.EmployeeIDOriginal
	INNER JOIN DimCustomer as c on c.CustomerIDOriginal = o.CustomerID
	INNER JOIN DimShipper as s ON o.ShipVia = s.ShipperIDOriginal
	INNER JOIN DimCountry as co ON co.CountryIDOriginal = o.ShipCountry
		GROUP BY dt.DimTimeID, e.DimEmployeeID, c.DimCustomerID, s.DimShipperID, co.DimCountryID
		ORDER BY DimEmployeeID , DimTimeID;

select * from FactEmpTime;

 select DimEmployeeID, DimCustomerID,  COUNT(*) as AnzahlOrders  from FactEmpTime GROUP BY DimEmployeeID, DimCustomerID ORDER BY DimEmployeeID;

 select DimCountryID, COUNT(*) as Zahl_L�nder from FactEmpTime Group by DimCountryID Order by DimCountryID;



 CREATE TABLE DimStandort
 (
	DimStandortID		INT IDENTITY PRIMARY KEY NONCLUSTERED,
	StandortIDOriginal	INT,
	RegionIDOriginal	INT
 );

 INSERT DimStandort
	SELECT
		TerritoryID,
		RegionID
	 FROM [Northwind].[dbo].[Territories];
		

INSERT FactNew
	SELECT
			dt.DimTimeID,
			e.DimEmployeeID,
			st.DimStandortID,
			COUNT(o.OrderID) as AnzahlOrders
			
	 FROM [Northwind].[dbo].[Orders] as o
	 INNER JOIN DimTime AS dt 
		ON dt.Quarter = DATEPART(Quarter,o.OrderDate) AND dt.Year = DATEPART(YEAR,o.OrderDate) AND dt.Month = DATEPART(month,o.OrderDate)
	INNER JOIN DimEmployee as e ON o.EmployeeID = e.EmployeeIDOriginal
	INNER JOIN DimStandort as st ON 
		GROUP BY 
		ORDER BY
