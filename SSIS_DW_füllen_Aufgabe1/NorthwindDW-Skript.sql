
-- Erstellen eines Mini-DW basierend auf der Nordwind

CREATE DATABASE NorthwindDW1;
GO
USE [NorthwindDW1];
GO


-- Aufbau von 3 Dimensionen: Zeit, Kunde, Produkt


-- Erst die Zeit-Dimension erstellen. Zum start reichen uns die Infos bezogen auf Quartal und Jahr. 
-- Sp�tere Zeit-Dimensionen k�nnen umfangreicher werden (Tag - Woche - Monat - Quartal - Jahr, Feiertage, etc.) 
CREATE TABLE DimTime
(
	DimTimeID			INT	IDENTITY PRIMARY KEY NONCLUSTERED, -- Immer daran denken, jede Dimension ben�tigt ihren eigenen Key
	[Quarter]			INT	NOT NULL,
	[Year]				INT	NOT NULL
);
GO		


-- Die Kunden Dimension. alle f�r uns wichtigen Informationen bezogen auf 
-- einen Kunden sollen in dieser Tabelle zusammengefasst werden
CREATE TABLE DimCustomer
(
	DimCustomerID		INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	CustomerIDBK		NCHAR(5)	NOT NULL, -- Nicht vergessen den Origial-Key mitzunehmen, aber nicht als Referenz sondern als Klartextwert
	
	-- Hier die eigentlichen Daten 
	[CompanyName]		NVARCHAR(40)	NOT NULL,
	[City]				NVARCHAR(15)	NULL,
	[Region]			NVARCHAR(15)	NULL,
	[Country]			NVARCHAR(15)	NULL
);
GO


-- Wie bei dem Kunden werden nun Informationen zu den Produkten zusammen gesamelt
CREATE TABLE DimProduct
(
	DimProductID		INT	IDENTITY PRIMARY KEY NONCLUSTERED,
	ProductIDBK			INT				NOT NULL,

	-- Produktinformationen
	[ProductName]		NVARCHAR(40)	NOT NULL,
	[CategoryName]		NVARCHAR(15)	NULL,
	[UnitPrice]		MONEY		NULL,
	[QuantityPerUnit]	NVARCHAR(20)	NULL
);
GO



-- Aufbau der Faktentabelle. Die Faktentabelle ben�tigt die Foreign-Keys der Dimensionen. 
-- In manchen Faktrentabellen sine wenige, in anderen viele zusatzinformationen enthalten
-- Diese hat nur eine Zusatzinformation : SalesAmount
CREATE TABLE FactSales
(
	DimTimeID			INT,
	DimCustomerID		INT,
	DimProductID		INT,

	PRIMARY KEY (DimTimeID, DimCustomerID, DimProductID), -- Der PK der Faktentabelle ist zusammengesetzt aus dem DimKeys

	SalesAmount			MONEY	NOT NULL,	-- Einziger relevanter Wert

	-- Alle Dim-Keys m�ssen rein
	FOREIGN KEY (DimTimeID) REFERENCES DimTime(DimTimeID),
	FOREIGN KEY (DimCustomerID) REFERENCES DimCustomer(DimCustomerID),
	FOREIGN KEY (DimProductID) REFERENCES DimProduct(DimProductID)
);
