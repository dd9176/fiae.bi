CREATE DATABASE PART_Test;
GO
USE PART_Test;

ALTER DATABASE PART_Test ADD FILEGROUP [unter10];
ALTER DATABASE PART_Test ADD FILEGROUP [�ber10];

ALTER DATABASE PART_Test ADD FILE (NAME = N'unter10', FILENAME = N'C:\Test\unter10') TO FILEGROUP [unter10];
ALTER DATABASE PART_Test ADD FILE (NAME = N'�ber10', FILENAME = N'C:\Test\�ber10') TO FILEGROUP [�ber10];

CREATE PARTITION FUNCTION Storage (int) AS RANGE RIGHT FOR VALUES ('10');

CREATE PARTITION SCHEME StorageSchema AS PARTITION Storage TO ('unter10', '�ber10');

CREATE TABLE TestData
(
	ID INT IDENTITY NOT NULL,
	Name NVARCHAR (10),
	Anzahl INT NOT NULL
) ON StorageSchema(Anzahl);



ALTER TABLE TestData ADD CONSTRAINT Pk_TestData PRIMARY KEY CLUSTERED (ID, Anzahl);

INSERT INTO TestData VALUES
('Hans', 5), ('Klaus', 12), ('Brigitte', 3), ('Peter', 45), ('Jutta', 8), ('Franz', 17), ('Simone', 2);

SELECT * FROM TestData

SELECT
sys.filegroups.name 'Storage' , sys.partitions.rows '7'
FROM
sys.partitions INNER JOIN sys.allocation_units
ON sys.allocation_units.container_id = sys.partitions.hobt_id
INNER JOIN sys.filegroups ON sys.filegroups.data_space_id =
sys.allocation_units.data_space_id
WHERE sys.partitions.object_id = OBJECT_ID('dbo.TestData')  
 

